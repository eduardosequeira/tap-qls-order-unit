"""Stream type classes for tap-qls."""

from pathlib import Path
from typing import Any, Optional, Dict, cast

import requests
from singer_sdk import typing as th

from tap_qls.client import QlsStream, QlsV2Stream


class ProductsStream(QlsStream):
    name = "products"
    path = "/fulfillment/products"
    primary_keys = ["id"]
    replication_key = "modified"
    records_jsonpath = "$.data[*]"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("company_id", th.StringType),
        th.Property("collection_id", th.StringType),
        th.Property("dimensions", th.CustomType({"type": ["object", "string"]})),
        th.Property("article_number", th.StringType),
        th.Property("ean", th.StringType),
        th.Property("name", th.StringType),
        th.Property("sku", th.StringType),
        th.Property("image_url", th.StringType),
        th.Property("country_code_of_origin", th.StringType),
        th.Property("hs_code", th.StringType),
        th.Property("need_barcode_picking", th.BooleanType),
        th.Property("need_best_before_stock", th.BooleanType),
        th.Property("need_serial_number", th.BooleanType),
        th.Property("amount_available", th.IntegerType),
        th.Property("amount_reserved", th.IntegerType),
        th.Property("amount_total", th.IntegerType),
        th.Property("price_cost", th.NumberType),
        th.Property("price_store", th.NumberType),
        th.Property("weight", th.IntegerType),
        th.Property("created", th.DateTimeType),
        th.Property("modified", th.DateTimeType),
        th.Property("foldable", th.BooleanType),
        th.Property(
            "suppliers",
            th.ArrayType(
                th.ObjectType(
                    th.Property("company_id", th.StringType),
                    th.Property("id", th.StringType),
                    th.Property("name", th.StringType),
                    th.Property(
                        "_joinData",
                        th.ObjectType(
                            th.Property("fulfillment_product_id", th.StringType),
                            th.Property("id", th.IntegerType),
                            th.Property("supplier_id", th.StringType),
                        ),
                    ),
                ),
            ),
        ),
        th.Property("barcodes", th.ArrayType(th.ObjectType(
            th.Property("barcode", th.StringType),
            th.Property("company_id", th.StringType),
            th.Property("created", th.DateTimeType),
            th.Property("fulfillment_product_id", th.StringType),
            th.Property("id", th.IntegerType),
            th.Property("modified", th.DateTimeType),
        ))),
        th.Property("image_url_handheld", th.StringType),
        th.Property("barcodes_and_ean", th.ArrayType(th.StringType)),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "product_id": record["id"],
        }


class SellOrdersStream(QlsStream):
    name = "orders"
    path = "/fulfillment/orders"
    primary_keys = ["id"]
    replication_key = "modified"
    records_jsonpath = "$.data[*]"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("customer_reference", th.StringType),
        th.Property("amount_delivered", th.IntegerType),
        th.Property("amount_reserved", th.IntegerType),
        th.Property("amount_total", th.IntegerType),
        th.Property("status", th.StringType),
        th.Property("created", th.DateTimeType),
        th.Property("modified", th.DateTimeType),
        th.Property("cancelled", th.StringType),
        th.Property("hold", th.BooleanType),
        th.Property("processable", th.DateTimeType),
        th.Property("shop_integration_id", th.StringType),
        th.Property("shop_integration_reference", th.StringType),
        th.Property("need_customs_information", th.BooleanType),
        th.Property(
            "receiver_contact",
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("companyname", th.StringType),
                th.Property("name", th.StringType),
                th.Property("street", th.StringType),
                th.Property("housenumber", th.StringType),
                th.Property("address2", th.StringType),
                th.Property("postalcode", th.StringType),
                th.Property("locality", th.StringType),
                th.Property("country", th.StringType),
                th.Property("email", th.StringType),
            ),
        ),
        th.Property(
            "delivery_options",
            th.ArrayType(th.CustomType({"type": ["object", "string"]})),
        ),
        th.Property(
            "brand",
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("name", th.StringType),
            ),
        ),
        th.Property(
            "replenishment_order_products",
            th.ArrayType(th.CustomType({"type": ["object", "string"]})),
        ),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "sell_order_id": record["id"],
        }


class SellOrdersByIdStream(QlsStream):
    name = "sell_orders_by_id"
    path = "/fulfillment/orders/{sell_order_id}"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = SellOrdersStream
    records_jsonpath = "$.data[*]"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("company_id", th.StringType),
        th.Property("shop_integration_id", th.StringType),
        th.Property("shop_integration_reference", th.StringType),
        th.Property("customer_reference", th.StringType),
        th.Property("customer_remarks", th.StringType),
        th.Property("brand_id", th.StringType),
        th.Property("receiver_contact_id", th.StringType),
        th.Property("shipment_configuration_id", th.StringType),
        th.Property("hold", th.BooleanType),
        th.Property("partical_delivery_allowed", th.BooleanType),
        th.Property("amount_delivered", th.IntegerType),
        th.Property("amount_reserved", th.IntegerType),
        th.Property("amount_total", th.IntegerType),
        th.Property("servicepoint_code", th.StringType),
        th.Property("status", th.StringType),
        th.Property("total_price", th.NumberType),
        th.Property("customs_invoice_number", th.StringType),
        th.Property("customs_shipment_type", th.StringType),
        th.Property("cancelled", th.StringType),
        th.Property("created", th.DateTimeType),
        th.Property("modified", th.DateTimeType),
        th.Property("processable", th.DateTimeType),
        th.Property("deleted", th.StringType),
        th.Property("need_customs_information", th.BooleanType),
        th.Property("return_shipments", th.ArrayType(th.CustomType({"type": ["object", "string"]}))),
        th.Property("shipment_configuration", th.CustomType({"type": ["object", "string"]})),
        th.Property("receiver_contact", th.ObjectType(
            th.Property("id", th.StringType),
            th.Property("company_id", th.StringType),
            th.Property("label", th.StringType),
            th.Property("name", th.StringType),
            th.Property("companyname", th.StringType),
            th.Property("street", th.StringType),
            th.Property("housenumber", th.StringType),
            th.Property("address2", th.StringType),
            th.Property("postalcode", th.StringType),
            th.Property("locality", th.StringType),
            th.Property("state_code", th.StringType),
            th.Property("country", th.StringType),
            th.Property("email", th.DateTimeType),
            th.Property("phone", th.DateTimeType),
            th.Property("vat", th.DateTimeType),
            th.Property("eori", th.StringType),
            th.Property("oss", th.StringType),
            th.Property("editable", th.BooleanType),
            th.Property("type", th.StringType),
            th.Property("created", th.DateTimeType),
            th.Property("modified", th.DateTimeType),
        )),
        th.Property(
            "products",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.StringType),
                    th.Property("company_id", th.StringType),
                    th.Property("order_id", th.StringType),
                    th.Property("product_id", th.StringType),
                    th.Property("name", th.StringType),
                    th.Property("shop_integration_reference", th.StringType),
                    th.Property("status", th.StringType),
                    th.Property("amount_open", th.IntegerType),
                    th.Property("amount_ordered", th.IntegerType),
                    th.Property("amount_reserved", th.IntegerType),
                    th.Property("amount_delivered", th.IntegerType),
                    th.Property("amount_original", th.IntegerType),
                    th.Property("country_code_of_origin", th.StringType),
                    th.Property("hs_code", th.StringType),
                    th.Property("price_per_unit", th.NumberType),
                    th.Property("custom1", th.CustomType({"type": ["object", "string"]})),
                    th.Property("special_handling", th.CustomType({"type": ["object", "string"]})),
                    th.Property("weight_per_unit", th.IntegerType),
                    th.Property("created", th.DateTimeType),
                    th.Property("modified", th.DateTimeType),
                    th.Property("currency", th.StringType),
                    th.Property(
                        "product",
                        th.ObjectType(
                            th.Property("id", th.StringType),
                            th.Property("company_id", th.StringType),
                            th.Property("collection_id", th.StringType),
                            th.Property(
                                "dimensions",
                                th.CustomType({"type": ["object", "string"]}),
                            ),
                            th.Property("article_number", th.StringType),
                            th.Property("ean", th.StringType),
                            th.Property("name", th.StringType),
                            th.Property("sku", th.StringType),
                            th.Property("image_url", th.StringType),
                            th.Property("country_code_of_origin", th.StringType),
                            th.Property("hs_code", th.StringType),
                            th.Property("need_barcode_picking", th.BooleanType),
                            th.Property("need_best_before_stock", th.BooleanType),
                            th.Property("need_serial_number", th.BooleanType),
                            th.Property("amount_available", th.IntegerType),
                            th.Property("amount_reserved", th.IntegerType),
                            th.Property("amount_total", th.IntegerType),
                            th.Property("price_cost", th.NumberType),
                            th.Property("price_store", th.NumberType),
                            th.Property("weight", th.IntegerType),
                            th.Property("created", th.DateTimeType),
                            th.Property("modified", th.DateTimeType),
                            th.Property("foldable", th.BooleanType),
                            th.Property("bundle_products", th.ArrayType(th.CustomType({"type": ["object", "string"]}))),
                            th.Property("image_url_handheld", th.StringType),
                            th.Property(
                                "barcodes_and_ean",
                                th.ArrayType(th.StringType),
                            ),
                        ),
                    ),
                )
            ),
        ),
        th.Property(
            "events",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.StringType),
                    th.Property("order_id", th.StringType),
                    th.Property("delivery_id", th.StringType),
                    th.Property("message", th.StringType),
                    th.Property("category", th.StringType),
                    th.Property("created", th.DateTimeType),
                    th.Property("creator_name", th.StringType),
                )
            ),
        ),
        th.Property("delivery_options", th.ArrayType(th.CustomType({"type": ["object", "string"]}))),
        th.Property("deliveries", th.ArrayType(th.CustomType({"type": ["object", "string"]}))),
        th.Property("brand", th.ObjectType(
            th.Property("id", th.StringType),
            th.Property("company_id", th.StringType),
            th.Property("gb_eori", th.StringType),
            th.Property("gb_vat", th.StringType),
            th.Property("name", th.StringType),
            th.Property("website", th.StringType),
            th.Property("logo_web_image_id", th.StringType),
            th.Property("logo_print_image_id", th.StringType),
            th.Property("sender_contact_id", th.StringType),
            th.Property("return_contact_id", th.StringType),
            th.Property("tracking_notification_policy", th.StringType),
            th.Property("tracking_first_notification_on", th.StringType),
            th.Property("fulfillment_allow_auto_print", th.BooleanType),
            th.Property("fulfillment_allow_partical_delivery", th.BooleanType),
            th.Property("fulfillment_deliveryslip_bottom_text", th.StringType),
            th.Property("fulfillment_deliveryslip_mode", th.StringType),
            th.Property("fulfillment_force_servicepoint", th.BooleanType),
            th.Property("fulfillment_myorder_allow_servicepoint", th.BooleanType),
            th.Property("fulfillment_myorder_allow_timeframe", th.BooleanType),
            th.Property("fulfillment_pack_deliveryslip", th.BooleanType),
            th.Property("fulfillment_pack_deliveryslip_ondemand", th.BooleanType),
            th.Property("fulfillment_allow_email_delivery_timeframe", th.BooleanType),
            th.Property("return_enabled", th.BooleanType),
            th.Property("return_free", th.BooleanType),
            th.Property("return_free_countries", th.CustomType({"type": ["array", "string"]})),
            th.Property("return_shop_integration_id", th.StringType),
            th.Property("return_received_email", th.BooleanType),
            th.Property("return_reasons", th.CustomType({"type": ["object", "string"]})),
            th.Property("return_terms", th.CustomType({"type": ["object", "string"]})),
            th.Property("return_within_days", th.IntegerType),
            th.Property("return_action_after_delivery_fail", th.StringType),
            th.Property("return_action_after_retour_term", th.StringType),
            th.Property("return_action_default", th.StringType),
            th.Property("with_return_label", th.BooleanType),
            th.Property("created", th.DateTimeType),
            th.Property("modified", th.DateTimeType),
            th.Property("deleted", th.DateTimeType),
        )),
    ).to_dict()


class BuyOrdersStream(QlsStream):
    name = "buy_orders"
    path = "/fulfillment/purchase-orders"
    primary_keys = ["id"]
    replication_key = "modified"
    records_jsonpath = "$.data[*]"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("customer_reference", th.StringType),
        th.Property("amount_expected", th.IntegerType),
        th.Property("amount_received", th.IntegerType),
        th.Property("preorder_allowed", th.BooleanType),
        th.Property("supplier_name", th.StringType),
        th.Property("status", th.StringType),
        th.Property("created", th.DateTimeType),
        th.Property("modified", th.DateTimeType),
        th.Property("cancelled", th.StringType),
        th.Property("expected", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "purchase_order_id": record["id"],
        }


class BuyOrdersByIdStream(QlsStream):
    name = "buy_orders_by_id"
    path = "/fulfillment/purchase-orders/{purchase_order_id}"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = BuyOrdersStream
    records_jsonpath = "$.data[*]"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("customer_reference", th.StringType),
        th.Property("customer_remarks", th.StringType),
        th.Property("amount_received", th.IntegerType),
        th.Property("amount_expected", th.IntegerType),
        th.Property("supplier_name", th.StringType),
        th.Property("preorder_allowed", th.BooleanType),
        th.Property("cancelled", th.StringType),
        th.Property("expected", th.StringType),
        th.Property("created", th.DateTimeType),
        th.Property("modified", th.DateTimeType),
        th.Property(
            "receipts",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.StringType),
                    th.Property("receiving_company_id", th.StringType),
                    th.Property("status", th.StringType),
                    th.Property("remarks", th.StringType),
                    th.Property("created", th.DateTimeType),
                    th.Property("modified", th.DateTimeType),
                    th.Property("received", th.StringType),
                    th.Property("closed", th.StringType),
                    th.Property(
                        "receipt_products",
                        th.ArrayType(th.CustomType({"type": ["object", "string"]})),
                    ),
                    th.Property(
                        "receipt_boxes",
                        th.ArrayType(th.CustomType({"type": ["object", "string"]})),
                    ),
                    th.Property(
                        "receipt_attachments",
                        th.ArrayType(th.CustomType({"type": ["object", "string"]})),
                    ),
                )
            ),
        ),
        th.Property(
            "products",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.StringType),
                    th.Property("product_id", th.StringType),
                    th.Property("amount_expected", th.IntegerType),
                    th.Property("amount_received", th.IntegerType),
                    th.Property("created", th.DateTimeType),
                    th.Property("modified", th.DateTimeType),
                    th.Property(
                        "product",
                        th.ObjectType(
                            th.Property("id", th.StringType),
                            th.Property("company_id", th.StringType),
                            th.Property("collection_id", th.StringType),
                            th.Property(
                                "dimensions",
                                th.CustomType({"type": ["object", "string"]}),
                            ),
                            th.Property("article_number", th.StringType),
                            th.Property("ean", th.StringType),
                            th.Property("name", th.StringType),
                            th.Property("sku", th.StringType),
                            th.Property("image_url", th.StringType),
                            th.Property("country_code_of_origin", th.StringType),
                            th.Property("hs_code", th.StringType),
                            th.Property("need_barcode_picking", th.BooleanType),
                            th.Property("need_best_before_stock", th.BooleanType),
                            th.Property("need_serial_number", th.BooleanType),
                            th.Property("amount_available", th.IntegerType),
                            th.Property("amount_reserved", th.IntegerType),
                            th.Property("amount_total", th.IntegerType),
                            th.Property("price_cost", th.NumberType),
                            th.Property("price_store", th.NumberType),
                            th.Property("weight", th.IntegerType),
                            th.Property("created", th.DateTimeType),
                            th.Property("modified", th.DateTimeType),
                            th.Property("foldable", th.BooleanType),
                            th.Property("barcodes", th.ArrayType(th.ObjectType(
                                th.Property("barcode", th.StringType),
                                th.Property("company_id", th.StringType),
                                th.Property("created", th.DateTimeType),
                                th.Property("fulfillment_product_id", th.StringType),
                                th.Property("id", th.IntegerType),
                                th.Property("modified", th.DateTimeType),
                            ))),
                            th.Property("bundle_products", th.ArrayType(th.CustomType({"type": ["object", "string"]}))),
                            th.Property("image_url_handheld", th.StringType),
                            th.Property(
                                "barcodes_and_ean",
                                th.ArrayType(th.StringType),
                            ),
                        ),
                    ),
                )
            ),
        ),
        th.Property(
            "events",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.StringType),
                    th.Property("receipt_id", th.StringType),
                    th.Property("message", th.StringType),
                    th.Property("created", th.DateTimeType),
                    th.Property("creator_name", th.StringType),
                )
            ),
        ),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        ids = list(map(lambda receipt: receipt["id"], list(record["receipts"])))

        if len(ids):
            return {
                "purchase_order_id": record["id"],
                "receipt_ids": ids,
                "first_receipt_id": ids[0],
            }
        else:
            return None

    def _sync_children(self, child_context: dict) -> None:
        for child_stream in self.child_streams:
            if child_stream.selected or child_stream.has_selected_descendents:
                if child_context is not None:
                    child_stream.sync(context=child_context)


class ReceiptLinesStream(QlsStream):
    name = "receipt_lines"
    parent_stream_type = BuyOrdersByIdStream
    primary_keys = ["id"]
    path = "/fulfillment/purchase-orders/{purchase_order_id}/receipts/"
    records_jsonpath = "$.data[*]"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("purchase_order_id", th.StringType),
        th.Property("company_id", th.StringType),
        th.Property("status", th.StringType),
        th.Property("remarks", th.CustomType({"type": ["object", "string"]})),
        th.Property("created", th.DateTimeType),
        th.Property("modified", th.DateTimeType),
        th.Property("received", th.StringType),
        th.Property("closed", th.StringType),
        th.Property("receiving_company_id", th.StringType),
        th.Property(
            "receipt_attachments",
            th.ArrayType(th.CustomType({"type": ["object", "string"]})),
        ),
        th.Property(
            "receipt_boxes", th.ArrayType(th.CustomType({"type": ["object", "string"]}))
        ),
        th.Property(
            "receipt_products",
            th.ArrayType(th.CustomType({"type": ["object", "string"]})),
        ),
    ).to_dict()

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        receipt_ids = self.ids
        ids_len = len(receipt_ids)
        previous_token = previous_token or 0
        if ids_len > 0 and previous_token < ids_len - 1:
            next_page_token = previous_token + 1
            return next_page_token
        else:
            return None

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params=""
        if context:
            self.ids = context["receipt_ids"]
            index = next_page_token or 0
            params =  self.ids[index]
        return params

    def prepare_request(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> requests.PreparedRequest:

        http_method = self.rest_method
        params: str = self.get_url_params(context, next_page_token)
        url: str = self.get_url(context) + params
        request_data = self.prepare_request_payload(context, next_page_token)
        headers = self.http_headers

        authenticator = self.authenticator
        if authenticator:
            headers.update(authenticator.auth_headers or {})

        request = cast(
            requests.PreparedRequest,
            self.requests_session.prepare_request(
                requests.Request(
                    method=http_method,
                    url=url,
                    headers=headers,
                    json=request_data,
                ),
            ),
        )
        return request

class SuppliersStream(QlsStream):
    name = "suppliers"
    path = "/suppliers"
    primary_keys = ["id"]
    replication_key = None
    records_jsonpath = "$.data[*]"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("company_id", th.StringType),
        th.Property("name", th.StringType),
    ).to_dict()


class SupplierProductStream(QlsStream):
    name = "supplier_products"
    path = "/fulfillment/products/{product_id}"
    primary_keys = ["id"]
    replication_key = None
    records_jsonpath = "$.data[*]"
    parent_stream_type = ProductsStream
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("company_id", th.StringType),
        th.Property("collection_id", th.StringType),
        th.Property("dimensions", th.CustomType({"type": ["object", "string"]})),
        th.Property("article_number", th.StringType),
        th.Property("ean", th.StringType),
        th.Property("name", th.StringType),
        th.Property("sku", th.StringType),
        th.Property("image_url", th.StringType),
        th.Property("country_code_of_origin", th.StringType),
        th.Property("hs_code", th.StringType),
        th.Property("need_barcode_picking", th.BooleanType),
        th.Property("need_best_before_stock", th.BooleanType),
        th.Property("need_serial_number", th.BooleanType),
        th.Property("amount_available", th.IntegerType),
        th.Property("amount_total", th.IntegerType),
        th.Property("price_cost", th.NumberType),
        th.Property("price_store", th.NumberType),
        th.Property("weight", th.IntegerType),
        th.Property("created", th.DateTimeType),
        th.Property("modified", th.DateTimeType),
        th.Property("foldable", th.BooleanType),
        th.Property("suppliers", th.ArrayType(
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("company_id", th.StringType),
                th.Property("name", th.StringType),
                th.Property("_joinData", th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("fulfillment_product_id", th.StringType),
                    th.Property("supplier_id", th.StringType),
                    th.Property("supplier_code", th.StringType),
                )),
            )
        )),
        th.Property("vas", th.ArrayType(th.CustomType({"type": ["object", "string"]}))),
        th.Property("product_master_cartons", th.ArrayType(
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("product_id", th.StringType),
                th.Property("amount", th.IntegerType),
                th.Property("ean", th.StringType),
                th.Property("creator", th.StringType),
                th.Property("creator_company_id", th.StringType),
                th.Property("creator_user_id", th.StringType),
                th.Property("created", th.StringType),
                th.Property("modified", th.StringType),
                th.Property("deleted", th.StringType),
            )
        )),
        th.Property("product_measurements", th.ArrayType(th.CustomType({"type": ["object", "string"]}))),
        th.Property("barcodes", th.ArrayType(th.ObjectType(
            th.Property("barcode", th.StringType),
            th.Property("company_id", th.StringType),
            th.Property("created", th.DateTimeType),
            th.Property("fulfillment_product_id", th.StringType),
            th.Property("id", th.IntegerType),
            th.Property("modified", th.DateTimeType),
        ))),
        th.Property("warehouse_stock_transfers", th.ArrayType(th.CustomType({"type": ["object", "string"]}))),
        th.Property("bundle_products", th.ArrayType(th.CustomType({"type": ["object", "string"]}))),
        th.Property("warehouse_stocks", th.ArrayType(th.CustomType({"type": ["object", "string"]}))),
        th.Property("mappings", th.ArrayType(th.ObjectType(
            th.Property("created", th.DateTimeType),
            th.Property("deleted", th.BooleanType),
            th.Property("id", th.StringType),
            th.Property("modified", th.DateTimeType),
            th.Property("product_id", th.StringType),
            th.Property("shop_integration", th.ObjectType(
                th.Property("brand_id", th.StringType),
                th.Property("company_id", th.StringType),
                th.Property("created", th.DateTimeType),
                th.Property("deleted", th.BooleanType),
                th.Property("fulfillment_product_create_new", th.BooleanType),
                th.Property("fulfillment_product_match_by_ean", th.BooleanType),
                th.Property("fulfillment_product_match_by_name", th.BooleanType),
                th.Property("fulfillment_product_match_by_sku", th.BooleanType),
                th.Property("fulfillment_product_match_fields", th.StringType),
                th.Property("fulfillment_sync_orders", th.BooleanType),
                th.Property("fulfillment_sync_products", th.BooleanType),
                th.Property("fulfillment_sync_stock", th.BooleanType),
                th.Property("id", th.StringType),
                th.Property("last_imported", th.DateTimeType),
                th.Property("modified", th.DateTimeType),
                th.Property("name", th.StringType),
                th.Property("type_id", th.IntegerType),
            )),
            th.Property("shop_integration_id", th.StringType),
            th.Property("shop_integration_reference", th.StringType),
            th.Property("shop_integration_reference2", th.StringType),
            th.Property("stock", th.IntegerType),
            th.Property("sync_stock", th.BooleanType),
            ))),
        th.Property("order_unit", th.IntegerType),
        th.Property("image_url_handheld", th.StringType),
        th.Property("barcodes_and_ean", th.ArrayType(th.StringType))
    ).to_dict()

class BackordersStream(QlsStream):
    name = "backorders"
    path = "/fulfillment/products/backorders"
    primary_keys = None
    replication_key = None
    records_jsonpath = "$.data[*]"
    schema = th.PropertiesList(
        th.Property("amount", th.IntegerType),
        th.Property("product", th.ObjectType(
            th.Property("id", th.StringType),
            th.Property("name", th.StringType),
            th.Property("ean", th.StringType),
            th.Property("sku", th.StringType),
        )),
    ).to_dict()


class BuyOrdersV2Stream(QlsV2Stream):
    name = "buy_orders_v2"
    path = "/purchase-orders/"
    primary_keys = ["id"]
    records_jsonpath = "$.data[*]"
    replication_key = "modified"

    schema = th.PropertiesList(
        th.Property("id", th.CustomType({"type": ["string", "number"]})),
        th.Property("company_id", th.StringType),
        th.Property("customer_title", th.StringType),
        th.Property("status", th.StringType),
        th.Property("pre_order", th.BooleanType),
        th.Property("cancelled", th.DateTimeType),
        th.Property("modified", th.DateTimeType),
        th.Property("archived", th.DateTimeType),
        th.Property("created", th.DateTimeType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "purchase_order_id_v2": record["id"],
        }


class BuyOrdersByIdV2Stream(QlsV2Stream):
    name = "buy_orders_by_id_v2"
    path = "/purchase-orders/{purchase_order_id_v2}"
    primary_keys = ["id"]
    parent_stream_type = BuyOrdersV2Stream
    records_jsonpath = "$.data[*]"

    schema = th.PropertiesList(
        th.Property("id", th.CustomType({"type": ["string", "number"]})),
        th.Property("company_id", th.StringType),
        th.Property("customer_title", th.StringType),
        th.Property("status", th.StringType),
        th.Property("pre_order", th.BooleanType),
        th.Property("cancelled", th.DateTimeType),
        th.Property("archived", th.DateTimeType),
        th.Property("modified", th.DateTimeType),
        th.Property("created", th.DateTimeType),
        th.Property(
            "purchase_order_labels",
            th.ArrayType(
                th.ObjectType(
                    th.Property("purchase_order_id", th.CustomType({"type": ["string", "number"]})),
                    th.Property("number", th.IntegerType),
                    th.Property("created", th.DateTimeType),
                    th.Property("received", th.DateTimeType),
                    th.Property("created", th.DateTimeType),
                    th.Property(
                        "purchase_order_label_products",
                        th.ArrayType(th.CustomType({"type": ["object", "string"]})),
                    ),
                )
            ),
        ),
        th.Property(
            "purchase_order_deliveries",
            th.ArrayType(
                th.ObjectType(
                    th.Property("purchase_order_id", th.CustomType({"type": ["string", "number"]})),
                    th.Property("barcode", th.StringType),
                    th.Property("estimated_arrival", th.DateTimeType),
                    th.Property("received", th.DateTimeType),
                    th.Property("created", th.DateTimeType),
                )
            ),
        ),
        th.Property(
            "purchase_order_suppliers",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.CustomType({"type": ["string", "number"]})),
                    th.Property("purchase_order_id", th.CustomType({"type": ["string", "number"]})),
                    th.Property("supplier_id", th.CustomType({"type": ["string", "number"]})),
                    th.Property(
                        "supplier",
                        th.ObjectType(
                            th.Property("name", th.StringType),
                        ),
                    ),
                )
            ),
        ),
        th.Property(
            "purchase_order_products",
            th.ArrayType(
                th.ObjectType(
                    th.Property("purchase_order_id", th.CustomType({"type": ["string", "number"]})),
                    th.Property("fulfillment_product_id", th.StringType),
                    th.Property("amount", th.IntegerType),
                    th.Property("status", th.StringType),
                    th.Property("created", th.DateTimeType),
                    th.Property(
                        "fulfillment_product",
                        th.ObjectType(
                            th.Property("id", th.StringType),
                            th.Property("ean", th.StringType),
                            th.Property("sku", th.StringType),
                            th.Property("name", th.StringType),
                            th.Property("image_url_handheld", th.StringType),
                            th.Property(
                                "barcodes_and_ean",
                                th.ArrayType(th.StringType),
                            ),
                        ),
                    ),
                )
            ),
        ),
    ).to_dict()

class ProductsStockStream(QlsStream):
    name = "products_stock"
    path = "/fulfillment/products/stock"
    modified_less_than = "filter[greater_than_modified]"
    modified_greater_than = "filter[less_than_modified]"
    primary_keys = ["ean"]
    replication_key = "modified"
    records_jsonpath = "$.data[*]"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("ean", th.StringType),
        th.Property("sku", th.StringType),
        th.Property("modified", th.DateTimeType),
        th.Property("created", th.DateTimeType),
        th.Property("amount_total", th.IntegerType),
        th.Property("amount_reserved", th.IntegerType),
        th.Property("amount_available", th.IntegerType),
        th.Property("amount_salable", th.IntegerType),
        th.Property("amount_sold", th.IntegerType),
        th.Property("amount_backorder", th.IntegerType),
        th.Property("amount_forecast", th.IntegerType),
        th.Property("amount_internally_moving", th.IntegerType),
        th.Property("amount_prognose", th.IntegerType),
        th.Property("amount_incoming", th.IntegerType),
        th.Property("amount_preorderable", th.IntegerType),
    ).to_dict()