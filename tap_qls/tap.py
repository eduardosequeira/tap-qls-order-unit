"""Qls tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_qls.streams import (
    BuyOrdersByIdStream,
    BuyOrdersStream,
    ProductsStream,
    ReceiptLinesStream,
    SellOrdersStream,
    SuppliersStream,
    SupplierProductStream,
    SellOrdersByIdStream,
    BackordersStream,
    BuyOrdersV2Stream,
    BuyOrdersByIdV2Stream,
    ProductsStockStream
)

STREAM_TYPES = [
    ProductsStream,
    SellOrdersStream,
    BuyOrdersStream,
    BuyOrdersByIdStream,
    ReceiptLinesStream,
    SuppliersStream,
    SupplierProductStream,
    SellOrdersByIdStream,
    BackordersStream,
    BuyOrdersV2Stream,
    BuyOrdersByIdV2Stream,
    ProductsStockStream
]


class TapQls(Tap):
    """Qls tap class."""

    name = "tap-qls"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property(
            "username",
            th.StringType,
            required=True,
            description="The token to authenticate against the API service",
        ),
        th.Property(
            "password",
            th.StringType,
            required=True,
            description="Project IDs to replicate",
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
        th.Property(
            "company_id",
            th.StringType,
            required=True,
            description="Company id",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapQls.cli()